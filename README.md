# BI

## INTRODUCCIÓN AL BUSINESS INTELLIGENCE

### FUENTE

* [Primera parte][bi03]: Universitat Oberta de Catalunya (2014.11.05)

### DEFINICIÓN

* Tomar datos históricos para proyectar el futuro a través del análisis de la información y conocimiento generados
* Captura, procesamiento y explotación de datos para toma de decisiones

### INFORMACIÓN

* Todos la utilizan a distinto nivel para tomar decisiones en base a sus necesidades
* Se identifican **tres** grupos:
  * **Alta dirección**: Información agregada y des-estructurada para decisiones estratégicas de mercado y productos
  * **Mandos intermedios**: Información operativa y semi-estructurada para indicadores de seguimiento y gestión
  * **Equipos y empleados**; Información estructurada para decisiones de protocolos y reglas

### TECNOLOGÍA

* Permite manejar altos volúmenes de datos de distinta tipología generados interna y externamente en nuestro entorno
* De los **datos operativos**, a través de una máquina **ETL** y *middlewares* (integradores) se generan **metadatos** (datos físicos en repositorios BI con visión lógica). Los metadatos pasan a **aplicaciones** con programas de integración y generan *querys*, informes o cubos OLAP, lo es entregado a la capa de **presentación**

### ÁREAS

* Se tienen múltiples tipos de usuarios, roles y perfiles que trabajan bajo el marco del BI, algunos ejemplos:
  * **Desarrolladores de integración** y **arquitectos de soluciones** (escriben *scripts* de extracción de datos). **Analistas de negocio** y **administradores de base de datos** (entienden requerimientos y construyen el modelo). **Gestores de proyectos** y **preparadores de informes** (presentan resultados con herramientas)
* En general hay tres tipos de perfiles, los que tienen información **cualitativa/cuantitativa**, los que tienen información **técnica/tecnológica** y los que tienen información de **negocio** (todos dependen entre sí y son importantes)
* Un profesional de BI debe ser "todo terreno", y tener habilidades en las siguientes áreas:
	1. **El negocio**: Conoce lo suficiente del sector y funciones empresariales, con conocimiento de estadística y gestión de proyectos
	2. **La tecnología**: Conoce de bases de datos, *data warehouse*, ETL, redes, aplicaciones y rendimientos
	3. **Habilidades interpersonales**: Posee habilidades transversales de comunicación, colaboración, liderazgo, creatividad, disciplina y pasión
* Citando a Thomas H. Davenport (Oct. 2012):

> Gran parte del entusiasmo actual por los *big data* se enfocan en la tecnología. Aunque la tecnología es importante, como mínimo es igual de importante la gente con las capacidades (o la mentalidad) para usarla. Y en este frente, la demanda es mayor que la oferta

[bi03]: https://vimeo.com/110989177